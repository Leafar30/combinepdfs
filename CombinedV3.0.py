import os
from PyPDF2 import PdfFileMerger
from tkinter import filedialog, messagebox, simpledialog
from pathlib import Path
from tkinter import *

suffix = '.pdf'
home = str(Path.home())

root = Tk()
root.withdraw()
root.pdfs = filedialog.askopenfilenames(initialdir = "C:/",title = "Please select the PDF files")

messagebox.showinfo("Information", "Your file will be saved on this path" + " " +  home)

filename = simpledialog.askstring("PDF Name", "Please name your PDF file", parent=root)

merger = PdfFileMerger()

for pdf in root.pdfs:
    merger.append(pdf)

merger.write(os.path.join(home, filename + suffix))
merger.close()